# #########################################
# VARIABLES DEFINITION
# #########################################

variable "GOOGLE_CREDENTIALS" { type = any }
variable "workspace" {type = string }

variable "project_id" { type = string }
variable "folder_id" { type = string }
variable "project_name" { type = string }
variable "region" { type = string }
# variable "creds" { type = string }
variable "org_id" { type = string }
variable "billing_account" { type = string }

variable "buckets" {
  type = map(string)
  default = {
    landing = "landing-zone"
    raw     = "raw-zone"
    curated = "curated-zone"
    backend = "tf-backend"
  }
  description = "Buckets Map"
}

variable "common_labels" {
  type = map(string)
  default = {
    owner       = "marco_marulli"
    deployed_by = "terraform"
  }
  description = "List of Common Labels"
}