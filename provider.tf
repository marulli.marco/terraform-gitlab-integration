terraform {
  required_version = "~> 0.12.6"
  required_providers {
    google = "3.11"
  }
}

provider "google" {
  # credentials = file(var.creds)
  project     = var.project_id
  region      = var.region
}
