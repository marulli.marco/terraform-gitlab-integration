# #########################################
# OUTPUTS
# #########################################

output "project" {
  value = module.project.project_id
}

output "storage_module" {
  value = [for b in module.cloud-storage.created_buckets : b.name]
}
