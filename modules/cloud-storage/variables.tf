variable "project_id" {
  type        = string
  default     = ""
  description = "Project ID"
}

variable "suffix" {
  type        = string
  default     = ""
  description = "Bucket Name Suffix"
}

variable "region" {
  type        = string
  default     = ""
  description = "Region"
}

variable "buckets_map" {
  type        = map
  default     = {}
  description = "Buckets Map"
}

variable "common_labels" {
  type        = map(string)
  default     = {}
  description = "List of Common Labels"
}