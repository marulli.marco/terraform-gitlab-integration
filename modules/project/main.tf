# #########################################
# PROJECT MODULE
# #########################################

locals {
  admin_enabled_apis = [
    "cloudresourcemanager.googleapis.com",
    "cloudbilling.googleapis.com",
    "serviceusage.googleapis.com",
    "storage-api.googleapis.com", // Google Cloud Storage JSON API
  ]
}

resource "google_project" "project" {
  name                = var.project_name
  project_id          = var.project_id
  folder_id           = var.folder_id
  billing_account     = var.billing_account
  auto_create_network = "false"
}

resource "google_project_service" "enabled-apis" {
  for_each                   = toset(local.admin_enabled_apis)
  project                    = var.project_id
  service                    = each.value
  disable_dependent_services = true
  disable_on_destroy         = true
  depends_on                 = [google_project.project]
}